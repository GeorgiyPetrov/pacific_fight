﻿using Microsoft.EntityFrameworkCore;
using pf_core_db.DbContextNmsp;
using pf_core_db.Entities;
using pf_repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pf_repository.Auth
{
    public class AuthRepository : IAuthRepository
    {

        private readonly AppDbContext _db;

        public AuthRepository(AppDbContext db)
        {
            _db = db;
        }


        public async Task<User> CreateUser(string login, string password, string name)
        {
            var user = new User() { Id = Guid.NewGuid(), Login = login, Name = name, PasswordHash = password};
            _db.Users.Add(user);
            await _db.SaveChangesAsync();
            return user;
        }

        public async Task<User> GetUserById(Guid userId)
        {
            return await _db.Users.FindAsync(userId);
        }

        public async Task<User> GetUserByLogin(string login)
        {
            return await _db.Users.FirstOrDefaultAsync(e => e.Login == login);
        }
    }
}
