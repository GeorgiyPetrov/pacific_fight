﻿using pf_core_db.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pf_repository.Interfaces
{
    public interface IAuthRepository
    {
        Task<User> GetUserByLogin(string login);

        Task<User> GetUserById(Guid userId);

        Task<User> CreateUser(string login, string password, string name);

    }
}
