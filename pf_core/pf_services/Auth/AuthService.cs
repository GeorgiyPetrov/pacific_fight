﻿using Microsoft.Extensions.Logging;
using pf_core_db.Entities;
using pf_repository.Interfaces;
using pf_services.Responsesmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pf_services.Auth
{
    public class AuthService : IAuthService
    {

        private readonly ILogger<AuthService> _logger;
        private readonly IAuthRepository _authRepo;
        public AuthService(ILogger<AuthService> logger, IAuthRepository authRepo)
        {
            _logger = logger;
            _authRepo = authRepo;
        }

        public async Task<BaseResponseModel<User>> LoginUser(string login, string password)
        {
            var user = await _authRepo.GetUserByLogin(login);
            if (user == null || user.PasswordHash != password)
                return new BaseResponseModel<User>("Пользователь не найден или пароль не подошел");
            else
                return new BaseResponseModel<User>(user);
        }

        public async Task<BaseResponseModel<User>> TryCreateUser(string login, string password, string name)
        {
            try
            {
                var user = await _authRepo.CreateUser(login, password, name);
                return new BaseResponseModel<User>(user);
            }
            catch (Exception ex)
            {
                return new BaseResponseModel<User>("Произола ошибка при создании пользвателя (");
                _logger.LogError($"Error in TryCreateUser {ex.Message}\n{ex.StackTrace}");
            }
        }
    }
}
