﻿using pf_core_db.Entities;
using pf_services.Responsesmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pf_services.Auth
{
    public interface IAuthService
    {
        /// <summary>
        /// Вернет пользователя если успешно или выбросит ошибку
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<BaseResponseModel<User>> LoginUser(string login, string password);

        /// <summary>
        /// Создаст пользователя
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<BaseResponseModel<User>> TryCreateUser(string login, string password, string name); 
    }
}
