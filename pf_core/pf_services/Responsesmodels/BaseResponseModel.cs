﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pf_services.Responsesmodels
{
    public class BaseResponseModel<T> where T: class
    {
        public T Content { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }

        public BaseResponseModel(string error)
        {
            this.Error = error;
            this.Success = false;
        }

        public BaseResponseModel()
        {
            Success = true;
        }

        public BaseResponseModel(T content)
        {
            this.Content = content;
            Success = true;
        }
    }
}
