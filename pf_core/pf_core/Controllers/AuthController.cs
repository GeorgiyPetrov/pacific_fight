﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using pf_repository.Interfaces;
using pf_services.Auth;
using System.Security.Claims;

namespace pf_core.Controllers
{
    [ApiController]

    [Route("api/auth")]
    public class AuthController : ControllerBase
    {

        private readonly IAuthRepository _authRepo;
        private readonly IAuthService _authService;

        public AuthController(IAuthRepository authRepo, IAuthService authService)
        {
            _authRepo = _authRepo;
            _authService = authService;
        }

        [HttpGet("login")]
        public async Task<IActionResult> Login(string login, string password)
        {
            var validateUserResult = await _authService.LoginUser(login, password);
            if (!validateUserResult.Success) return BadRequest(validateUserResult);

            var claims = new List<Claim>() { new Claim(ClaimsIdentity.DefaultNameClaimType, validateUserResult.Content.Id.ToString()) };
            var identity = new ClaimsIdentity(claims);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity));
            return Ok(validateUserResult);
        }


        [HttpGet("register")]
        public async Task<IActionResult> Register(string login, string password, string userName)
        {
            var user = await _authService.TryCreateUser(login, password, userName);

            return Ok(user);
        }
    }
}
