﻿using pf_repository.Auth;
using pf_repository.Interfaces;
using pf_services.Auth;

namespace pf_core
{
    public static class Startup
    {
        public static void AddStartup(this WebApplicationBuilder builder)
        {

            if (builder == null)
            {
                return;
            }

            builder.Services.AddAuthentication().AddCookie(option => {
                option.Cookie.Name = "pacific_fight_identity";
                option.Cookie.SameSite = SameSiteMode.Strict;
                option.Cookie.HttpOnly = true;
                option.SlidingExpiration = true;
                option.ExpireTimeSpan = TimeSpan.FromDays(120);
                option.Cookie.MaxAge = TimeSpan.FromDays(120);
            });

            builder.Services.AddControllers();

            var CorsAllowedOrigins = "_corsAllowed";
            builder.Services.AddCors(options =>
            {
                options.AddPolicy(CorsAllowedOrigins,
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });

            builder.Services.AddScoped<IAuthRepository, AuthRepository>();
            builder.Services.AddScoped<IAuthService, AuthService>();

            builder.Services.AddSwaggerGen();
            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            
            builder.Services.AddMemoryCache();
        }
    }
}
